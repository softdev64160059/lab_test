/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.sasipa.lab3_3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Lenovo
 */
public class OXProgarm {
    
    public OXProgarm() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinNoPlayBY_O_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false,Lab3.checkWin(table, currentPlayer));
        
    }
    @Test
    public void testCheckWinRowBY_O_output_true() {
        String[][] table = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWinRow1BY_X_output_true() {
        String[][] table = {{"X", "X", "X"}, {"-", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWinRow2BY_O_output_true() {
        String[][] table = {{"O", "O", "O"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWinRow3BY_X_output_true() {
        String[][] table = {{"O", "O", "O"}, {"-", "-", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWinColBY_O_output_true() {
        String[][] table = {{"O", "-", "-"}, {"O", "-", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
     @Test
    public void testCheckWinCol1BY_X_output_true() {
        String[][] table = {{"X", "-", "-"}, {"X", "-", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWinCol2BY_O_output_true() {
        String[][] table = {{"-", "O", "-"}, {"-", "O", "-"}, {"-", "O", "-"}};
        String currentPlayer = "O";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
     @Test
    public void testCheckWinCol3BY_X_output_true() {
        String[][] table = {{"-", "X", "-"}, {"-", "X", "-"}, {"-", "X", "-"}};
        String currentPlayer = "X";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
     @Test
    public void testCheckWinCol4BY_O_output_true() {
        String[][] table = {{"-", "-", "O"}, {"-", "-", "O"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
     @Test
    public void testCheckWinCol5BY_X_output_true() {
        String[][] table = {{"-", "-", "X"}, {"-", "-", "X"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWindiagonalBY_O_output_true() {
        String[][] table = {{"O", "-", "-"}, {"-", "O", "-"}, {"-", "-", "O"}};
        String currentPlayer = "O";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWindiagonal1BY_X_output_true() {
        String[][] table = {{"X", "-", "-"}, {"-", "X", "-"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWindiagonal2BY_O_output_true() {
        String[][] table = {{"-", "-", "O"}, {"-", "O", "-"}, {"O", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckWindiagonal3BY_X_output_true() {
        String[][] table = {{"-", "-", "X"}, {"-", "X", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true,Lab3.checkWin(table, currentPlayer));
}
    @Test
    public void testCheckDraw1By_O_output_true() {
        String[][] table = {{"O", "X", "O",}, {"X", "O", "X",}, {"X", "O", "X",}};
        String currentPlayer = "O";
        assertEquals(true, Lab3.isDraw(table, currentPlayer));
    }
     @Test
    public void testCheckDraw2By_X_output_true() {
        String[][] table = {{"X", "O", "X",}, {"O", "X", "O",}, {"O", "X", "O",}};
        String currentPlayer = "X";
        assertEquals(true, Lab3.isDraw(table, currentPlayer));
    }
}
